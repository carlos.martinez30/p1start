package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList {
	private static CircularSortedDoublyLinkedList<Car> carList;
	private static CarList singleton = new CarList();
	
	private CarList() {
		carList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	}
	public static CircularSortedDoublyLinkedList<Car> getInstance(){
		return carList;
	}
	public static void resetCars() {
		carList.clear();
	}
}
