package edu.uprm.cse.datastructures.cardealer.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	private static class Node<E>{
		private E element;
		private Node<E> next;
		private Node<E> prev;
		
		public Node() {
			this.element = null;
			this.next = this.prev = null;
		}
		public Node(E e) {
			this.element = e;
			this.next = this.prev = null;
		}
		public E getElement() {
			return element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public Node<E> getPrev() {
			return prev;
		}
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}
		
	}
	
	private Node<E> header;
	private int currentSize;
	private Comparator<E> comp;
	
	public CircularSortedDoublyLinkedList(Comparator c) {
		this.currentSize = 0;
		this.comp = c;
		this.header = new Node<E>();
		this.header.setNext(this.header);
		this.header.setPrev(this.header);	
	}
	
	@Override
	public Iterator iterator() {
		return this.toArray().iterator(); // I used java's iterator
	}
	 
	@Override
	public boolean add(E obj) {
		Node<E> newObj = new Node<E>(obj);
		Node<E> temp = this.header.getNext();
		if(this.isEmpty()) {
			this.header.setNext(newObj);
			this.header.setPrev(newObj);
			newObj.setNext(this.header);
			newObj.setPrev(this.header);
			this.currentSize++;
			return true;
		}
		while(temp != this.header && comp.compare(temp.getElement(), obj)<0) { //Since its sorted, we must check where the element will be inserted.
			temp = temp.getNext();
		}
		newObj.setNext(temp);
		newObj.setPrev(temp.getPrev());
		temp.getPrev().setNext(newObj);
		temp.setPrev(newObj);
		this.currentSize++;
		return true;
		
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean remove(E obj) {
		Node<E> temp = this.header.next;
		if(this.isEmpty())return false;
		while(temp!=this.header) {
			if(temp.element.equals(obj)){
				temp.getPrev().setNext(temp.getNext());
				temp.getNext().setPrev(temp.getPrev());
				this.currentSize--;
				return true;
			}
			temp = temp.getNext();
		}
		return false;
	}

	@Override
	public boolean remove(int index) {
        Node<E> temp = this.header.getNext();
		if (index < 0 || index >= this.currentSize) {
			throw new IndexOutOfBoundsException("Remove: " + index);
		}
        for (int i = 0; i < index; i++){
        	temp = temp.getNext();
        }
        temp.getPrev().setNext(temp.getNext());
        temp.getNext().setPrev(temp.getPrev());
        currentSize--;
        return true;
	}
	        
	 

	@Override
	public int removeAll(E obj) {
		int cnt = 0;
		while(remove(obj)) cnt++;
		return cnt;
	}

	@Override
	public E first() {
		if(this.isEmpty())return null;
		return this.header.getNext().getElement();
	}

	@Override
	public E last() {
		if(this.isEmpty())return null;
		return this.header.getPrev().getElement();
	}

	@Override
	public E get(int index) {
		if(this.isEmpty())return null;
		Node<E> temp = this.header.getNext();
		for(int i = 0; i < this.currentSize; i++) {
			if(i == index) {
				return temp.getElement();
			}
			temp = temp.getNext();
		}
		return null;
	}

	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}
	}

	@Override
	public boolean contains(E e) {
		Node<E> temp = this.header.getNext();
		while(temp != this.header){
			if(temp.getElement().equals(e)) return true;
			temp = temp.getNext();
		}
		return false;
	}

	@Override
	public boolean isEmpty() {
		return this.currentSize==0;
	}

	@Override
	public int firstIndex(E e) {
		Node<E> temp = this.header.getNext();
		int i = 0;
		while(temp != this.header) {
			if(temp.getElement().equals(e))return i;
			i++;
			temp = temp.getNext();
		}
		return -1;
	}

	@Override
	public int lastIndex(E e) {
		Node<E> temp = this.header.getPrev();
		int i = this.currentSize-1;
		while(temp != this.header) {
			if(temp.getElement().equals(e))return i;
			i--;
			temp = temp.getPrev();
		}
		return -1;
	}
	public ArrayList<E> toArray(){//Helper method for iterator that returns an arraylist
		ArrayList<E> a = new ArrayList<E>();
		for(int i = 0; i < this.currentSize; i++) {
			a.add(this.get(i));
		}
		return a;
	}
}
