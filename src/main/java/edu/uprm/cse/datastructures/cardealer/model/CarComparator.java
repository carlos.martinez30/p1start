package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car> {
	
	@Override
	public int compare(Car o1, Car o2) {
		String car1 = ((Car)o1).getCarBrand() + ((Car)o1).getCarModel() + ((Car)o1).getCarModelOption();
		String car2 = ((Car)o2).getCarBrand() + ((Car)o2).getCarModel() + ((Car)o2).getCarModelOption();
		return car1.compareTo(car2);
	}
}
