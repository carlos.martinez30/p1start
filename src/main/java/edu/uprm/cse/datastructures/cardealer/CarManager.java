package edu.uprm.cse.datastructures.cardealer;

import java.util.Comparator;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import edu.uprm.cse.datastructures.cardealer.model.*;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/cars")
public class CarManager {
	private final CircularSortedDoublyLinkedList<Car> inventory = CarList.getInstance();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] inventoryArray = new Car[inventory.size()];
		for (int i = 0; i < inventory.size(); i++) {
			inventoryArray[i] = inventory.get(i);
		}
		return inventoryArray;
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){ 
		for(int i = 0; i < inventory.size(); i++){
			if(inventory.get(i).getCarId() == id){ 
				return inventory.get(i);
			}  
		}  
		throw new NotFoundException();
	}
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
    public Response addCar(Car car){
		
	for(int i = 0; i < inventory.size(); i++){
		if(inventory.get(i).getCarId() == car.getCarId() || inventory.get(i).getCarBrand()== null || inventory.get(i).getCarModel() == null || inventory.get(i).getCarModelOption() == null){
			return Response.status(Response.Status.CONFLICT).build();
		}
	}
	inventory.add(car);
	 return Response.status(Response.Status.CREATED).build();
	}
	
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car){
		for(int i=0; i<inventory.size(); i++){
			if(inventory.get(i).getCarId()==car.getCarId()){ 
				inventory.remove(i);
				inventory.add(car);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
	@DELETE
    @Path("/{id}/delete")
    public Response deleteCar(@PathParam("id") long id){
    	for(int i=0; i<inventory.size(); i++){
    		if(id==inventory.get(i).getCarId()){
    			inventory.remove(i);
    			return Response.status(Response.Status.OK).build();
    		}
    	}	
    	return Response.status(Response.Status.NOT_FOUND).build();
    }
}

